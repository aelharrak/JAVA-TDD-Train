package sii.maroc.dto;

import sii.maroc.Wagon;

public class WagonDto {
	
	public String wagonsOfTrain;
	public int index;
	public Wagon currentWagon;
	public boolean isAfirstWagon;
	public boolean isAHead;
	public int sizeOfWagons;
	

	public WagonDto(String wagonsOfTrain, Wagon currentWagon, boolean isAfirstWagon, boolean isAHead) {
		super();
		this.wagonsOfTrain = wagonsOfTrain;
		this.currentWagon = currentWagon;
		this.isAfirstWagon = isAfirstWagon;
		this.isAHead = isAHead;
	}



	public WagonDto(String wagonsOfTrain, int index, Wagon currentWagon, int sizeOfWagons, boolean isAHead) {
		this.wagonsOfTrain = wagonsOfTrain;
		this.index = index;
		this.currentWagon = currentWagon;
		this.sizeOfWagons = sizeOfWagons;
		this.isAHead = isAHead;
	}



}
