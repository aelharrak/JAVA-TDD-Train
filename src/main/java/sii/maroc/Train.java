package sii.maroc;

import java.util.LinkedList;
import sii.maroc.presentation.Helper;

public class Train {

	private static LinkedList<Wagon> wagons = new LinkedList<Wagon>();

	public Train(final String wagon) {
		wagons = TrainFactory.creatWagon(wagon);
	}

	public void detachEnd() {
		wagons.removeLast();
	}

	public void detachHead() {
		wagons.removeFirst();
	}

	public boolean fill() {
		return Helper.fill(wagons);
	}

	public Object print() {
		return Helper.print(wagons);
	}
}