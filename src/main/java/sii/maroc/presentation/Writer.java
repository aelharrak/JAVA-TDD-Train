package sii.maroc.presentation;

import sii.maroc.Wagon;

public class Writer {

	static String write(String wagonResult, final Wagon wagonTmp, final String start, final String end) {
		wagonResult +=  start + wagonTmp.getValue() + end;
		return wagonResult;
	}
}
