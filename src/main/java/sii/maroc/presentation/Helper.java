package sii.maroc.presentation;

import java.util.List;
import sii.maroc.Wagon;
import sii.maroc.dto.WagonDto;

public class Helper {

	public static String print(final List<Wagon> wagons) {

		String wagonsOfTrain = new String();
		WagonDto wagonDto;
		
		for (int index = 0; index < wagons.size(); index++) {

			Wagon currentWagon = wagons.get(index);
			int sizeOfWagons = wagons.size() - 1;
			boolean isAfirstWagon = (index == 0);
			boolean isAHead = isAHead(currentWagon);

			if (isAHead) {
				wagonDto = new WagonDto(wagonsOfTrain, currentWagon, isAfirstWagon, isAHead);
				wagonsOfTrain = printHeader(wagonDto);
			} else {
				wagonDto = new WagonDto(wagonsOfTrain, index, currentWagon, sizeOfWagons, isAHead);
				wagonsOfTrain = printQueue(wagonDto);
			}
		}

		return wagonsOfTrain;
	}

	private static String printHeader(final WagonDto wagonDto) {
		
		String wagonsOfTrain = wagonDto.wagonsOfTrain;
		Wagon currentWagon =  wagonDto.currentWagon;
		boolean isAfirstWagon = wagonDto.isAfirstWagon;
		
		if (isAfirstWagon) {
			return wagonsOfTrain = Writer.write(wagonsOfTrain, currentWagon, "<", "::");
		} else {
			return wagonsOfTrain = Writer.write(wagonsOfTrain, currentWagon, "", ">");
		}
	}

	private static String printQueue(final WagonDto wagonDto) {
		
		String wagonsOfTrain = wagonDto.wagonsOfTrain;
		int index = wagonDto.index;
		Wagon currentWagon = wagonDto.currentWagon;
		int sizeOfWagons = wagonDto.sizeOfWagons;
		boolean isAHead = wagonDto.isAHead;
		
		boolean isTheLastIndex = (index == sizeOfWagons);
		boolean isQueue = isTheLastIndex && !isAHead;
		
		if (isQueue) {
			return wagonsOfTrain = Writer.write(wagonsOfTrain, currentWagon, "|", "|");
		} else {
			return wagonsOfTrain = Writer.write(wagonsOfTrain, currentWagon, "|", "|::");
		}
	}

	private static boolean isAHead(final Wagon wagon) {
		return wagon == Wagon.H;
	}

	public static boolean fill(final List<Wagon> wagons) {
		for (int i = 0; i < wagons.size(); i++) {
			if (wagons.get(i) == Wagon.C) {
				wagons.set(i, Wagon.FC);
				return true;
			}
		}
		return false;
	}
}