package sii.maroc;

import java.util.LinkedList;
import sii.maroc.exception.TrainException;

public class TrainFactory {
	
	public static Wagon creat(final char wagon) throws TrainException{
		return getWagon(wagon);
	}

	public static Wagon getWagon(char wagon) throws TrainException {
		switch (wagon) {
			case 'H':
				return Wagon.H;
			case 'P':
				return Wagon.P;
			case 'C':
				return Wagon.C;
			case 'R':
				return Wagon.R;
			default:
				throw new TrainException("Cannot create a train with :"+ wagon);
			}
	}

	public static LinkedList<Wagon> creatWagon(final String wagon) {

		char[] wagons = wagon.toCharArray();
		LinkedList<Wagon> wagonResult = new LinkedList<Wagon>();
		
		for (int i = 0; i < wagons.length; i++) {
			try {
				char currentWagon = wagons[i];
				Wagon wagonToBeAddToTheTrain = creat(currentWagon);
				wagonResult.add(wagonToBeAddToTheTrain);
			} catch (TrainException e) {
				e.printStackTrace();
			}
		}
		return wagonResult;
	}
}