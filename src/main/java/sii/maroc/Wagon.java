package sii.maroc;

public enum Wagon {

	H("HHHH"),
	P("OOOO"),
	C("____"),
	FC("^^^^"),
	R("hThT");

	private String value;

	private Wagon(String wagon) {
		this.value = wagon;
	}

	public String getValue() {
		return this.value;
	}
}