package sii.maroc.exception;

@SuppressWarnings("serial")
public class TrainException extends Exception {

	public TrainException() {

	}
	public TrainException(final String message) {
		super(message);
	}
}